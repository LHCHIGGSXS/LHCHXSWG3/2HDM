#!/bin/bash


read_dir="./"
dflag=

while getopts d: name  # with no other args parses $@
do case $name in
     d) dflag=1
        read_dir="$OPTARG/";;
     *) echo "Usage: %s: [-d] <sushi input files>\n" $0
        exit 2;;
  esac
done
if [ "$dflag" ]; then
   echo "Option -d $read_dir specified";  fi
shift $(($OPTIND - 1))
echo "Remaining arguments are: $*"

for input_file in $*
do
    # construct the output file
    output_file=$(echo "$input_file" | sed 's/input_/output_/')

    # SusHi is limited to input and output file name lengths
    # <= 60 characters in length. Need to work around this
    new_input_file="/tmp/input_sushi_$$"
    new_output_file="/tmp/output_sushi_$$"
    
    dest_dir="sushi_files"
    if [ $(echo $output_file | grep -c "7TeV") -gt 0 ]; then
	dest_dir="${dest_dir}_7TeV"
    elif [ $(echo $output_file | grep -c "8TeV") -gt 0 ]; then
	dest_dir="${dest_dir}_8TeV"
    fi
    if [ $(echo $output_file | grep -c "type=1") -gt 0 ]; then
	dest_dir="${dest_dir}_type1"
    elif [ $(echo $output_file | grep -c "type=2") -gt 0 ]; then
	dest_dir="${dest_dir}_type2"
    fi



    /bin/cp -f ${read_dir}$input_file $new_input_file
    ./sushi $new_input_file $new_output_file
    /bin/mv -f $new_output_file ${dest_dir}/${output_file}
done
