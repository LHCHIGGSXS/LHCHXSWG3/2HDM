#!/usr/bin/env python

## interactive "submit" script
## author: Stephen Sekula (SMU)
##
## description:
##
## This script is designed to allow a user to run commands with
## arguments interactively, and can plug into an environment that
## normally expects a batch submission script to be called with
## these options.
##
## python ./interactive_submit.py -c '<COMMAND>' -a '<ARGUMENTS>' -o '<PATH>/<TO>/<LOG FILES>/<PREFIX>'
##
## This will run the command (placeholder <COMMAND> above) and
## do so by passing the given arguments (placeholder <ARGUMENTS> above) 
## to the command. For instance:
##
## python ./interactive_submit.py -c '/bin/ls' -a '-ltr'
##
## would run the UNIX "ls" command, with the arguments "-ltr".
##
## For usage instructions on the command line, you can run
## the following:
##
## python ./interactive_submit.py -h
##
##


# Import commands
import glob
import os
import sys
import getopt
import random
import time
import subprocess
import re


# The usage() function, for printing usage instructions
# on the command line

def usage():
    print ' \
interactive_submit.py <options> \n \
\n \
where <options> is one or more of . . . \n\
\n \
   -h                  Print help information (this info) \n \
   -c                  Command to execute \n \
   -a                  Arguments to pass to command \n \
   -o                  dummy argument - not used \n \
   -n                  dummy argument - not used \n \
'

    

# Main code
global _command
global _arguments
global _maxnumber
global _output

_command = ""
_arguments = ""
_maxnumber = -1
_output = ""

try:
    opts, args = getopt.getopt(sys.argv[1:], "hc:a:o:n:R", ["help", "command=", "arguments=", "output=", "maxnumber="])
except getopt.GetoptError:
    usage()
    sys.exit(2)
for opt, arg in opts:
    if opt in ("-h", "--help"):
        usage()
        sys.exit()
    elif opt in ("-c", "--command"):
        _command = arg
    elif opt in ("-a", "--arguments"):
        _arguments = arg
    elif opt in ("-o", "--output"):
        _output = arg
    elif opt in ("-n", "--maxnumber"):
        _maxnumber = arg


print 'Running command %s' % (_command)
print 'with arguments %s' % (_arguments)


subprocess.call('%s %s' % (_command, _arguments), shell=True)
    
    
