#!/usr/bin/env python


## condor_submit
## author: Stephen Sekula (SMU)
##
## description:
##
## This script is designed to allow a user to submit easy commands
## to the SMU HPC (condor-based) system. The syntax for executing
## the script is as follows:
##
## python ./condor_submit.py -c '<COMMAND>' -a '<ARGUMENTS>' -o '<PATH>/<TO>/<LOG FILES>/<PREFIX>'
##
## This will run the command (placeholder <COMMAND> above) on
## the HPC system and do so by passing the given arguments
## (placeholder <ARGUMENTS> above) to the command. For instance:
##
## python ./condor_submit.py -c '/bin/ls' -a '-ltr' -o '/tmp/ls-log-prefix'
##
## would run the UNIX "ls" command, with the arguments "-ltr",
## on the condor system. The condor config/log files discussing the process will
## be written to /tmp/ with the prefix 'ls-log-prefix' (e.g. /tmp/ls-log-prefix.err)
##
## When you run condor_submit, it creates the condor configuration
## file for you in a subdirectory of your current working directory.
## That subdirectory is named 'condor/' by default. If you look
## in the condor/ subdirectory, you will see directories there
## which are named using a nanosecond timestamp from the clock
## on the computer. Each call of the condor_submit command will
## result in a new subdirectory.
##
## Each job directory created under 'condor/' will contain the
## configuration files and the job output from each condor job.
## Try running the above simple example and look at the output
## that results. For more information on the syntax of the condor
## configuration file, and the condor output files, see the
## CONDOR system help website:
##
## http://www.cs.wisc.edu/condor/
##
## For usage instructions on the command line, you can run
## the following:
##
## python ./condor_submit.py -h
##
##


# Import commands
import glob
import os
import sys
import getopt
import random
import time
import commands
import re
from subprocess import call

# count the number of jobs in condor
def CondorJobCount():
    JobCountResponse = commands.getoutput('condor_q %s | grep %s | wc -l' % (os.environ['USER'],os.environ['USER']))
    while not JobCountResponse.isdigit():
        print " !!!!! CondorJobCount(): Received some garbage from condor. I'll try communicating again in a moment."
        JobCountResponse = CondorJobCount()
        time.sleep(10)
        pass
    
    return JobCountResponse

# The usage() function, for printing usage instructions
# on the command line

def usage():
    print ' \
condor_submit.py <options> \n \
\n \
where <options> is one or more of . . . \n\
\n \
   -h                  Print help information (this info) \n \
   -c                  Command to execute on Condor \n \
   -a                  Arguments to pass to command \n \
   -o                  Path to condor files and log files, and prefix at end of path \n \
   -n                  Max. number of jobs to put into the condor system \n \
   -R                  Retry condor jobs that fail in the system by auto-resubmitting. \n \
'

    

# Main body of the software.
# - processes arguments given to the script
# - creates the condor configuration file for the job
# - submits the job to condor

ID = '%15f' % (time.time())

global _command
global _arguments
global _maxnumber
global _output
global _retry

_command = ""
_arguments = ""
_maxnumber = -1
_output = "condor/%s" % (ID)
_retry = False

try:
    opts, args = getopt.getopt(sys.argv[1:], "hc:a:o:n:R", ["help", "command=", "arguments=", "output=", "maxnumber=", "retry"])
except getopt.GetoptError:
    usage()
    sys.exit(2)
for opt, arg in opts:
    if opt in ("-h", "--help"):
        usage()
        sys.exit()
    elif opt in ("-c", "--command"):
        _command = arg
    elif opt in ("-a", "--arguments"):
        _arguments = arg
    elif opt in ("-o", "--output"):
        _output = arg
    elif opt in ("-n", "--maxnumber"):
        _maxnumber = arg
    elif opt in ("-R", "--retry"):
        _retry = True


try:
    #dir = re.search(re.compile("(.*)\/.*"),_output).group(1)
    os.makedirs(_output)
except OSError:
    pass

WorkingDirectory=os.getcwd()


print 'Running command %s' % (_command)
print 'with arguments %s' % (_arguments)


JobPrefixName  = _output
CondorFileName = '%s/job.condor' % (JobPrefixName)

        
# copy the template D3PD config file to the condor job area and modify it for this
# job
        
condorfile = open(CondorFileName, 'w')

condorfilecontents = '\
Universe     = vanilla\n\
Executable   = %(Command)s\n\
Arguments    = %(Arguments)s\n\
initialdir   = %(WorkingDirectory)s\n\
Log          = %(JobPrefixName)s/job.log\n\
Output       = %(JobPrefixName)s/job.out\n\
Error        = %(JobPrefixName)s/job.err\n' % {'JobPrefixName': JobPrefixName,'WorkingDirectory': WorkingDirectory, 'Command': _command, 'Arguments': _arguments}

if _retry:
    condorfilecontents = condorfilecontents + '\
Requirements = Machine=!=LastRemoteHost\n\
on_exit_remove = (ExitBySignal == False) && (ExitCode == 0) \n' % {'JobPrefixName': JobPrefixName,'WorkingDirectory': WorkingDirectory, 'Command': _command, 'Arguments': _arguments}
    pass

condorfilecontents = condorfilecontents + '\
getenv       = true\n\
notification = error\n\
Queue\n' % {'JobPrefixName': JobPrefixName,'WorkingDirectory': WorkingDirectory, 'Command': _command, 'Arguments': _arguments}

condorfile.write(condorfilecontents)

condorfile.close()

if _maxnumber != -1 and random.uniform(0,10) > 7:
    print 'Checking to see how many jobs you currently have in the system...'
    NumberOfJobs = int(CondorJobCount())
    while NumberOfJobs >= int(_maxnumber):
        # wait and check; when few enough jobs are present, then submit
        print ' \
        ===> WAIT: more than %d jobs (I count %d) are currently in condor under your name.\n \
        pausing for 10 seconds . . . ' % (int(_maxnumber), NumberOfJobs)
        time.sleep(10)
        NumberOfJobs = int(CondorJobCount())
        pass
    pass
        
print 'condor_submit %s' % (CondorFileName)
CondorSubmitResponse = commands.getoutput('condor_submit %s' % (CondorFileName))

print "Response from Condor:"
print CondorSubmitResponse

while CondorSubmitResponse.find("ERROR") != -1:
    # try again!
    time.sleep(1.0)
    CondorSubmitResponse = commands.getoutput('condor_submit %s' % (CondorFileName))

    
    
