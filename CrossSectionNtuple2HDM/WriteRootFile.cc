//
#include <vector>
#include <map>
#include <iostream>
#include <string>
#include <fstream>
#include <iomanip> 
#include <unistd.h>
#include <stdio.h>

// Root related headers
#include "TString.h"
#include "TRegexp.h"
#include "TFile.h"
#include "TTree.h"
#include "TSystem.h"
#include "TObjString.h"
#include "TObjArray.h"
#include "TF1.h"
#include "TMath.h"

// 2HDMC related headers
#include "THDM.h"
#include "SM.h"
#include "Constraints.h"
#include "DecayTable.h"

// my headers
#include "vbfcalc.h"


using namespace std;

std::string _outputfile = "results_default.root";

enum ParNames { imh = 0, imH = 1, imA = 2, imC = 3, itb = 4, isba = 5, itype = 6, isqrts = 7 };


double mH=-1;
double mh=-1;
double mC=-1;
double mA=-1;
double tb=-1;
double cba=-1;
double sba=-1;

double m12_2=-1.;

double lambda=-1;
double lambdaA=-1;
double lambdaF=-1;
double alpha=-1;

double lambda1=-1;
double lambda2=-1;
double lambda3=-1;
double lambda4=-1;
double lambda5=-1;
double lambda6=-1;

double width_h=-1;
double width_H=-1;
double width_C=-1;
double width_A=-1;

double width_t=-1;

double xsec_h_gg=-1;
double xsec_h_bb=-1;
double xsec_h_bb_5FS=-1;
double xsec_h_bb_4FS=-1;
double xsec_h_VBF=-1;
double xsec_h_WH=-1;
double xsec_h_ZH=-1;
double xsec_h_ttH=-1;

double xsec_H_gg=-1;
double xsec_H_bb=-1;
double xsec_H_bb_5FS=-1;
double xsec_H_bb_4FS=-1;
double xsec_H_VBF=-1;
double xsec_H_WH=-1;
double xsec_H_ZH=-1;
double xsec_H_ttH=-1;

double xsec_A_gg=-1;
double xsec_A_bb=-1;
double xsec_A_bb_5FS=-1;
double xsec_A_bb_4FS=-1;
double xsec_A_VBF=-1;
double xsec_A_VH=-1;
double xsec_A_ttH=-1;

// 7 TeV cross sections
double xsec_7TeV_h_gg=-1;
double xsec_7TeV_h_bb=-1;
double xsec_7TeV_h_bb_5FS=-1;
double xsec_7TeV_h_bb_4FS=-1;
double xsec_7TeV_h_VBF=-1;
double xsec_7TeV_h_WH=-1;
double xsec_7TeV_h_ZH=-1;
double xsec_7TeV_h_ttH=-1;

double xsec_7TeV_H_gg=-1;
double xsec_7TeV_H_bb=-1;
double xsec_7TeV_H_bb_5FS=-1;
double xsec_7TeV_H_bb_4FS=-1;
double xsec_7TeV_H_VBF=-1;
double xsec_7TeV_H_WH=-1;
double xsec_7TeV_H_ZH=-1;
double xsec_7TeV_H_ttH=-1;

double xsec_7TeV_A_gg=-1;
double xsec_7TeV_A_bb=-1;
double xsec_7TeV_A_bb_5FS=-1;
double xsec_7TeV_A_bb_4FS=-1;
double xsec_7TeV_A_VBF=-1;
double xsec_7TeV_A_VH=-1;
double xsec_7TeV_A_ttH=-1;

double br_H_ZZ=-1;
double br_H_WW=-1;
double br_H_tautau=-1;
double br_H_gammagamma=-1;
double br_H_hh=-1;
double br_H_tt=-1;
double br_H_bb=-1;

double br_h_ZZ=-1;
double br_h_WW=-1;
double br_h_tautau=-1;
double br_h_gammagamma=-1;
double br_h_bb=-1;

double br_A_tautau=-1;
double br_A_hh=-1;
double br_A_tt=-1;
double br_A_Zh=-1;
double br_A_bb=-1;
double br_A_gammagamma=-1;

double br_C_Wh=-1;
double br_C_WH=-1;
double br_C_tb=-1;

double br_t_bC=-1; // t->bH+
double br_C_taunu=-1; // H+->taunu

double oblique_S = -999;
double oblique_T = -999;
double oblique_U = -999;
double oblique_V = -999;
double oblique_W = -999;
double oblique_X = -999;

int type = -1;
int status = -1;

// grid to be used in calculations
vector<double> v_mH;
vector<double> v_mA;
vector<double> v_mh;
vector<double> v_mC;
vector<double> v_tb;
vector<double> v_sba;
//vector<double> v_cba;
//vector<double> v_lam;
//vector<double> v_lamA;
//vector<double> v_lamF;

// 4FS Global Variables
TFile* _4fs_scalar_7tev = NULL;
TFile* _4fs_pseudoscalar_file = NULL;
TFile* _4fs_scalar_8tev = NULL;
TFile* _4fs_pseudoscalar_8tev = NULL;

std::map<std::string, TF1*>* _4fs_xsec = new std::map<std::string, TF1*>;


bool fileExists(const std::string& filename) {
  return (gSystem->AccessPathName(filename.c_str()) == 0);
}

std::string ReadNthLine(const std::string& filename, int N)
{
  // check whether filename exists
  std::ifstream in(filename.c_str());
  if (!in.good()) {
    cout << "Problem in " << filename << endl;
  }
  std::string s;
  //for performance
  int some_reasonable_max_line_length=999;
  s.reserve(some_reasonable_max_line_length);    

  //skip N lines
  // BUG: first, use scanf instead of getline.
  // (there are ways to parse fast for information)
  // second, read the whole file into a char* point
  //
  //cout << "DDD At file: " << filename << endl;
  for(int i = 0; i < N; ++i){
    std::getline(in, s);
    //cout << "DDD I read " << i << ": " << s << endl;
  }

  std::getline(in,s);

  in.close();

  return s; 
}
double getXSfromLine(std::string str) {
  //         1     1.51150255E+01   # ggh XS in pb
  //123456789012345678901234567890
  //         1     7.54637208E+01   # ggh XS in pb
  //cout << "DDDD STRIPPING: " << str << endl;
  str.replace(str.begin()+30,str.end(),"");
  str.replace(str.begin(),str.begin()+15,"");
  double tmp = (double)atof(str.c_str());
  return tmp;

}

double getXS_gg(std::string fname) {
//   cout << "Reading from file: " << fname << endl;
//   cout << "fname: " << fname << "  " <<  ReadNthLine(fname, 18) << endl;
//   cout << getXSfromLine( ReadNthLine(fname, 18)  ) << endl;
//  return  getXSfromLine( ReadNthLine(fname, 18)  );
  return  getXSfromLine( ReadNthLine(fname, 21)  ); // Sushi-1.3.0
}

double getXS_bb(std::string fname) {
//   cout << "Reading from file: " << fname << endl;
//   cout << "fname: " << fname << "  " <<  ReadNthLine(fname, 20) << endl;
//   cout << getXSfromLine( ReadNthLine(fname, 20)  ) << endl;
//  return  getXSfromLine( ReadNthLine(fname, 20)  );
  return  getXSfromLine( ReadNthLine(fname, 23)  ); // Sushi-1.3.0
}

string StringToDouble(double dbl) {
  std::ostringstream strs;
  strs << dbl;
  return strs.str();
}

string MatchFromUntil(const std::string& text, const std::string& start, const std::string& end) {
  // exclusive of the ends, return the string between "start" and "end" characters
  std::string match = "";

  bool saving = false;

  for (int i = 0; i < text.size(); i++) {
    if (saving == false && text[i] == start) {
      saving = true;
    } else if (saving == true && text[i] == end) {
      break;
    } else if (saving == true) {
      match += text[i];
    }
  }
  return match;
}

void get2HDMValues(const std::string& SushiOutputFileName, std::map<int,double>* ExtraParameters = NULL) {

  // This function employs globally-stored values for things like mh, mH, mC, sba, tb, m12_2

  cba  = sqrt(1-sba*sba); // by definition cba >= 0
  // function get 2HDMC
  double mh_ref = 125.; 
  // Create SM and set top mass to 175 GeV 
  //  SM sm;  sm.set_qmass_pole(6, 173.3);
  SM sm;
  sm.set_qmass_pole(6, 172.5);
  sm.set_qmass_pole(5, 4.72);
  sm.set_qmass_pole(4, 1.57);
  sm.set_lmass_pole(3, 1.77684);
  sm.set_alpha_s(0.119);
  sm.set_MZ(91.15349);
  sm.set_MW(80.36951);
  sm.set_gamma_Z(2.49581);
  sm.set_gamma_W(2.08856);
  sm.set_GF(1.16637E-5);
  sm.set_alpha(1./137.0359997);


  THDM model;
  model.set_SM(sm);
  //model.set_yukawas_type(type);
  bool pset = model.set_param_phys
    (mh,mH,mA,mC,sba,
     /*lambda_6*/ 0.,
     /*lambda_7*/ 0., m12_2,tb);
  if (!pset) {
    cout << "The specified parameters are not valid\n";
    status = 9999;
    return;
    //    abort();
  }

  model.set_yukawas_type(type);
  Constraints constr(model);
  double S,T,U,V,W,X;
  constr.oblique_param(mh_ref,S,T,U,V,W,X);
  status = 0;
  if ( !constr.check_stability()) { status += 1; }
  if ( !constr.check_unitarity()) { status += 2; }
  if ( !constr.check_perturbativity()) { status += 4; }
  if ( !constr.check_masses()) { status +=8; }

  oblique_S = S;
  oblique_T = T;
  oblique_U = U;
  oblique_V = V;
  oblique_W = W;
  oblique_X = X;

  DecayTable table(model);
  // (1,2,3,4 = h,H,A,H+)
  width_h = table.get_gammatot_h(1);
  width_H = table.get_gammatot_h(2);
  width_A = table.get_gammatot_h(3);
  width_C = table.get_gammatot_h(4);
  width_t = table.get_gammatot_top();
  //
  br_h_gammagamma = table.get_gamma_hgaga(1)/width_h;
  br_H_gammagamma = table.get_gamma_hgaga(2)/width_H;
  br_A_gammagamma = table.get_gamma_hgaga(3)/width_A;
  // 1,2,3 = gamma,Z,W
  br_h_ZZ = table.get_gamma_hvv(1,2)/width_h;
  br_H_ZZ = table.get_gamma_hvv(2,2)/width_H;
  br_h_WW = table.get_gamma_hvv(1,3)/width_h;
  br_H_WW = table.get_gamma_hvv(2,3)/width_H;
  //  cout << "Test: BR=" << br_h_ZZ << "  sba=" << sba << endl;
  // 1,2,3 = e,\mu,\tau 

  br_h_tautau =  table.get_gamma_hll(1,3,3)/width_h;
  br_H_tautau =  table.get_gamma_hll(2,3,3)/width_H;
  br_A_tautau =  table.get_gamma_hll(3,3,3)/width_A;

  br_A_tt =  table.get_gamma_huu(3,3,3)/width_A;
  br_H_tt =  table.get_gamma_huu(2,3,3)/width_H;

  br_A_bb =  table.get_gamma_hdd(3,3,3)/width_A;
  br_H_bb =  table.get_gamma_hdd(2,3,3)/width_H;
  br_h_bb =  table.get_gamma_hdd(1,3,3)/width_h;

  br_H_hh  = table.get_gamma_hhh(2,1,1)/width_H;
  br_A_hh  = table.get_gamma_hhh(3,1,1)/width_A;

  br_A_Zh  = table.get_gamma_hvh(3,2,1)/width_A;
  br_C_Wh  = table.get_gamma_hvh(3,3,1)/width_C;
  br_C_WH  = table.get_gamma_hvh(3,3,2)/width_C;
  br_C_tb  = table.get_gamma_hdu(4,3,3)/width_C;

  br_t_bC  = table.get_gamma_uhd(3,4,3)/width_t;
  br_C_taunu = table.get_gamma_hln(4,3,3)/width_C;

  double dummy_tb;
  model.get_param_HHG(lambda1, lambda2, lambda3, lambda4, lambda5, lambda6, dummy_tb);

  // calculate the VBF cross sections by simple scaling of the SM VBF cross sections
  xsec_h_VBF =  getVBFXSSM(mh) * sba*sba;
  xsec_H_VBF =  getVBFXSSM(mH) * cba*cba;
  xsec_7TeV_h_VBF =  getVBFXSSM_7TeV(mh) * sba*sba;
  xsec_7TeV_H_VBF =  getVBFXSSM_7TeV(mH) * cba*cba;

  xsec_h_WH =  getWHXSSM(mh) * sba*sba;
  xsec_H_WH =  getWHXSSM(mH) * cba*cba;
  xsec_7TeV_h_WH =  getWHXSSM_7TeV(mh) * sba*sba;
  xsec_7TeV_H_WH =  getWHXSSM_7TeV(mH) * cba*cba;

  xsec_h_ZH =  getZHXSSM(mh) * sba*sba;
  xsec_H_ZH =  getZHXSSM(mH) * cba*cba;
  xsec_7TeV_h_ZH =  getZHXSSM_7TeV(mh) * sba*sba;
  xsec_7TeV_H_ZH =  getZHXSSM_7TeV(mH) * cba*cba;


  // tt(h,H,A) are the same in all 2HDM types - I, II, lepton-specific, and flipped.
  double ksi_u_h = sba + cba / tb;
  double ksi_u_H = cba - sba / tb;
  double ksi_u_A = 1 / tb;

  xsec_h_ttH = getTTHXSSM(mh) * ksi_u_h * ksi_u_h;
  xsec_H_ttH = getTTHXSSM(mH) * ksi_u_H * ksi_u_H;
  xsec_A_ttH = getTTHXSSM(mA) * ksi_u_A * ksi_u_A;

  xsec_7TeV_h_ttH = getTTHXSSM_7TeV(mh) * ksi_u_h * ksi_u_h;
  xsec_7TeV_H_ttH = getTTHXSSM_7TeV(mH) * ksi_u_H * ksi_u_H;
  xsec_7TeV_A_ttH = getTTHXSSM_7TeV(mA) * ksi_u_A * ksi_u_A;

  // 2HDMC needs beta 0 - pi/2, -pi/2<beta-alpha<pi/2
  // calculate by hand alpha, such that cos(beta-alpha) > 0
  // beta-alpha = asin( sba ) -> alpha = beta - asin( sba )
  // alpha = atan( tb ) - asin( sba  )
  alpha = atan(tb) - asin(sba);
  string  s_alpha = StringToDouble(alpha);

  // now read the cross sections
  TString outputfile_7TeV_h = SushiOutputFileName;
  TString outputfile_7TeV_H;
  TString outputfile_7TeV_A;
  TString outputfile_8TeV_h;
  TString outputfile_8TeV_H;
  TString outputfile_8TeV_A;

  outputfile_7TeV_H = outputfile_7TeV_h;
  outputfile_7TeV_H.ReplaceAll("higgs=0","higgs=2");

  outputfile_7TeV_A = outputfile_7TeV_h;
  outputfile_7TeV_A.ReplaceAll("higgs=0","higgs=1");

  outputfile_8TeV_h = outputfile_7TeV_h;
  outputfile_8TeV_h.ReplaceAll("7TeV","8TeV");

  outputfile_8TeV_H = outputfile_7TeV_H;
  outputfile_8TeV_H.ReplaceAll("7TeV","8TeV");

  outputfile_8TeV_A = outputfile_7TeV_A;
  outputfile_8TeV_A.ReplaceAll("7TeV","8TeV");


  // Need to take into account the type of THDM model for these Hbb couplings/BFs
  xsec_h_bb_5FS =  getXS_bb(outputfile_8TeV_h.Data());
  xsec_H_bb_5FS =  getXS_bb(outputfile_8TeV_H.Data());
  xsec_A_bb_5FS =  getXS_bb(outputfile_8TeV_A.Data());

  if (type == 1 || type == 3) {
    ksi_u_h = sba + cba / tb;
    ksi_u_H = cba - sba / tb;
    ksi_u_A = 1 / tb;
  } else if (type == 2 || type == 4) {
    ksi_u_h = sba - cba * tb;
    ksi_u_H = cba + sba * tb;
    ksi_u_A = tb;
  }
  
  xsec_h_bb_4FS = (*_4fs_xsec)["scalar_8TeV"]->Eval(mh)*1.0e-3       * ksi_u_h * ksi_u_h;
  xsec_H_bb_4FS = (*_4fs_xsec)["scalar_8TeV"]->Eval(mH)*1.0e-3       * ksi_u_H * ksi_u_H;
  xsec_A_bb_4FS = (*_4fs_xsec)["pseudoscalar_8TeV"]->Eval(mA)*1.0e-3 * ksi_u_A * ksi_u_A;

  double mb_pole = 4.75;
  double w_h = TMath::Log(mh/mb_pole) - 2.0;
  double w_H = TMath::Log(mH/mb_pole) - 2.0;
  double w_A = TMath::Log(mA/mb_pole) - 2.0;

  xsec_h_bb = (xsec_h_bb_4FS + w_h*xsec_h_bb_5FS)/(1.+w_h);
  xsec_H_bb = (xsec_H_bb_4FS + w_h*xsec_H_bb_5FS)/(1.+w_H);
  xsec_A_bb = (xsec_A_bb_4FS + w_h*xsec_A_bb_5FS)/(1.+w_A);

  xsec_h_gg =  getXS_gg(outputfile_8TeV_h.Data());
  xsec_H_gg =  getXS_gg(outputfile_8TeV_H.Data());
  xsec_A_gg =  getXS_gg(outputfile_8TeV_A.Data());
  
  // 7 TeV
  xsec_7TeV_h_bb_5FS =  getXS_bb(outputfile_7TeV_h.Data());
  xsec_7TeV_H_bb_5FS =  getXS_bb(outputfile_7TeV_H.Data());
  xsec_7TeV_A_bb_5FS =  getXS_bb(outputfile_7TeV_A.Data());

  xsec_7TeV_h_bb_4FS = (*_4fs_xsec)["scalar_7TeV"]->Eval(mh)*1.0e-3       * ksi_u_h * ksi_u_h;
  xsec_7TeV_H_bb_4FS = (*_4fs_xsec)["scalar_7TeV"]->Eval(mH)*1.0e-3	  * ksi_u_H * ksi_u_H;
  xsec_7TeV_A_bb_4FS = (*_4fs_xsec)["pseudoscalar_7TeV"]->Eval(mA)*1.0e-3 * ksi_u_A * ksi_u_A;

  xsec_7TeV_h_bb = (xsec_7TeV_h_bb_4FS + w_h*xsec_7TeV_h_bb_5FS)/(1.+w_h);
  xsec_7TeV_H_bb = (xsec_7TeV_H_bb_4FS + w_h*xsec_7TeV_H_bb_5FS)/(1.+w_H);
  xsec_7TeV_A_bb = (xsec_7TeV_A_bb_4FS + w_h*xsec_7TeV_A_bb_5FS)/(1.+w_A);

  xsec_7TeV_h_gg =  getXS_gg(outputfile_7TeV_h.Data());
  xsec_7TeV_H_gg =  getXS_gg(outputfile_7TeV_H.Data());
  xsec_7TeV_A_gg =  getXS_gg(outputfile_7TeV_A.Data());



}

void WriteRootFile(const std::vector<string>& sushifiles) {

  TFile *f = new TFile(_outputfile.c_str(),"recreate");
  

  TTree *t = new TTree("thdm","2HDM parameters");
  t->Branch("mA",&mA,"mA/D");
  t->Branch("mH",&mH,"mH/D");
  t->Branch("mh",&mh,"mh/D");
  t->Branch("mCh",&mC,"mCh/D");
  t->Branch("tanb",&tb,"tanb/D");
  t->Branch("m12_2",&m12_2,"m12_2/D");

  t->Branch("sba",&sba,        "sba/D");
  t->Branch("cba",&cba,        "cba/D");
  t->Branch("lambda", &lambda, "lambda/D");
  t->Branch("lambdaF",&lambdaF,"lambdaF/D");
  t->Branch("lambdaA",&lambdaA,"lambdaA/D");
  t->Branch("alpha",&alpha,"alpha/D");
 
  t->Branch("lambda1", &lambda1, "lambda1/D");
  t->Branch("lambda2", &lambda2, "lambda2/D");
  t->Branch("lambda3", &lambda3, "lambda3/D");
  t->Branch("lambda4", &lambda4, "lambda4/D");
  t->Branch("lambda5", &lambda5, "lambda5/D");
  t->Branch("lambda6", &lambda6, "lambda6/D");

  t->Branch("width_h",&width_h,"width_h/D");
  t->Branch("width_H",&width_H,"width_H/D");
  t->Branch("width_A",&width_A,"width_A/D");
  t->Branch("width_Ch",&width_C,"width_Ch/D");
  t->Branch("width_t",&width_t,"width_t/D");


  t->Branch("xsec_h_gg",  &xsec_h_gg,  "xsec_h_gg/D");
  t->Branch("xsec_h_bb",  &xsec_h_bb,  "xsec_h_bb/D");
  t->Branch("xsec_h_bb_5FS",  &xsec_h_bb_5FS,  "xsec_h_bb_5FS/D");
  t->Branch("xsec_h_bb_4FS",  &xsec_h_bb_4FS,  "xsec_h_bb_4FS/D");
  t->Branch("xsec_h_VBF", &xsec_h_VBF, "xsec_h_VBF/D");
  t->Branch("xsec_h_WH",  &xsec_h_WH,  "xsec_h_WH/D");
  t->Branch("xsec_h_ZH",  &xsec_h_ZH,  "xsec_h_ZH/D");
  t->Branch("xsec_h_ttH",  &xsec_h_ttH,  "xsec_h_ttH/D");

  t->Branch("xsec_H_gg",  &xsec_H_gg,  "xsec_H_gg/D");
  t->Branch("xsec_H_bb",  &xsec_H_bb,  "xsec_H_bb/D");
  t->Branch("xsec_H_bb_5FS",  &xsec_H_bb_5FS,  "xsec_H_bb_5FS/D");
  t->Branch("xsec_H_bb_4FS",  &xsec_H_bb_4FS,  "xsec_H_bb_4FS/D");
  t->Branch("xsec_H_VBF", &xsec_H_VBF, "xsec_H_VBF/D");
  t->Branch("xsec_H_WH",  &xsec_H_WH,  "xsec_H_WH/D");
  t->Branch("xsec_H_ZH",  &xsec_H_ZH,  "xsec_H_ZH/D");
  t->Branch("xsec_H_ttH",  &xsec_H_ttH,  "xsec_H_ttH/D");

  t->Branch("xsec_A_gg",  &xsec_A_gg,  "xsec_A_gg/D");
  t->Branch("xsec_A_bb",  &xsec_A_bb,  "xsec_A_bb/D");
  t->Branch("xsec_A_bb_5FS",  &xsec_A_bb_5FS,  "xsec_A_bb_5FS/D");
  t->Branch("xsec_A_bb_4FS",  &xsec_A_bb_4FS,  "xsec_A_bb_4FS/D");
  t->Branch("xsec_A_VBF", &xsec_A_VBF, "xsec_A_VBF/D");
  t->Branch("xsec_A_VH",  &xsec_A_VH,  "xsec_A_VH/D");
  t->Branch("xsec_A_ttH",  &xsec_A_ttH,  "xsec_A_ttH/D");


  t->Branch("xsec_7TeV_h_gg",  &xsec_7TeV_h_gg,  "xsec_7TeV_h_gg/D");
  t->Branch("xsec_7TeV_h_bb",  &xsec_7TeV_h_bb,  "xsec_7TeV_h_bb/D");
  t->Branch("xsec_7TeV_h_bb_5FS",  &xsec_7TeV_h_bb_5FS,  "xsec_7TeV_h_bb_5FS/D");
  t->Branch("xsec_7TeV_h_bb_4FS",  &xsec_7TeV_h_bb_4FS,  "xsec_7TeV_h_bb_4FS/D");
  t->Branch("xsec_7TeV_h_VBF", &xsec_7TeV_h_VBF, "xsec_7TeV_h_VBF/D");
  t->Branch("xsec_7TeV_h_WH",  &xsec_7TeV_h_WH,  "xsec_7TeV_h_WH/D");
  t->Branch("xsec_7TeV_h_ZH",  &xsec_7TeV_h_ZH,  "xsec_7TeV_h_ZH/D");
  t->Branch("xsec_7TeV_h_ttH", &xsec_7TeV_h_ttH, "xsec_7TeV_h_ttH/D");

  t->Branch("xsec_7TeV_H_gg",  &xsec_7TeV_H_gg,  "xsec_7TeV_H_gg/D");
  t->Branch("xsec_7TeV_H_bb",  &xsec_7TeV_H_bb,  "xsec_7TeV_H_bb/D");
  t->Branch("xsec_7TeV_H_bb_5FS",  &xsec_7TeV_H_bb_5FS,  "xsec_7TeV_H_bb_5FS/D");
  t->Branch("xsec_7TeV_H_bb_4FS",  &xsec_7TeV_H_bb_4FS,  "xsec_7TeV_H_bb_4FS/D");
  t->Branch("xsec_7TeV_H_VBF", &xsec_7TeV_H_VBF, "xsec_7TeV_H_VBF/D");
  t->Branch("xsec_7TeV_H_WH",  &xsec_7TeV_H_WH,  "xsec_7TeV_H_WH/D");
  t->Branch("xsec_7TeV_H_ZH",  &xsec_7TeV_H_ZH,  "xsec_7TeV_H_ZH/D");
  t->Branch("xsec_7TeV_H_ttH", &xsec_7TeV_H_ttH, "xsec_7TeV_H_ttH/D");

  t->Branch("xsec_7TeV_A_gg",  &xsec_7TeV_A_gg,  "xsec_7TeV_A_gg/D");
  t->Branch("xsec_7TeV_A_bb",  &xsec_7TeV_A_bb,  "xsec_7TeV_A_bb/D");
  t->Branch("xsec_7TeV_A_bb_5FS",  &xsec_7TeV_A_bb_5FS,  "xsec_7TeV_A_bb_5FS/D");
  t->Branch("xsec_7TeV_A_bb_4FS",  &xsec_7TeV_A_bb_4FS,  "xsec_7TeV_A_bb_4FS/D");
  t->Branch("xsec_7TeV_A_VBF", &xsec_7TeV_A_VBF, "xsec_7TeV_A_VBF/D");
  t->Branch("xsec_7TeV_A_VH",  &xsec_7TeV_A_VH,  "xsec_7TeV_A_VH/D");
  t->Branch("xsec_7TeV_A_ttH", &xsec_7TeV_A_ttH, "xsec_7TeV_A_ttH/D");		 		    		      


  //  t->Branch("",  &,  "/D");
  t->Branch("br_H_ZZ",  &br_H_ZZ,  "br_H_ZZ/D");
  t->Branch("br_H_WW",  &br_H_WW,  "br_H_WW/D");
  t->Branch("br_H_tautau",  &br_H_tautau,  "br_H_tautau/D");
  t->Branch("br_H_gammagamma",  &br_H_gammagamma,  "br_H_gammagamma/D");
  t->Branch("br_H_hh",  &br_H_hh,  "br_H_hh/D");
  t->Branch("br_H_tt",  &br_H_tt,  "br_H_tt/D");
  t->Branch("br_H_bb",  &br_H_bb,  "br_H_bb/D");

  t->Branch("br_h_ZZ",  &br_h_ZZ,  "br_h_ZZ/D");
  t->Branch("br_h_WW",  &br_h_WW,  "br_h_WW/D");
  t->Branch("br_h_tautau",  &br_h_tautau,  "br_h_tautau/D");
  t->Branch("br_h_gammagamma",  &br_h_gammagamma,  "br_h_gammagamma/D");
  t->Branch("br_h_bb",  &br_h_bb,  "br_h_bb/D");

  t->Branch("br_A_tautau",  &br_A_tautau,  "br_A_tautau/D");
  t->Branch("br_A_hh",  &br_A_hh,  "br_A_hh/D");
  t->Branch("br_A_tt",  &br_A_tt,  "br_A_tt/D");
  t->Branch("br_A_Zh",  &br_A_Zh,  "br_A_Zh/D");
  t->Branch("br_A_bb",  &br_A_bb,  "br_A_bb/D");
  t->Branch("br_A_gammagamma",  &br_A_gammagamma,  "br_A_gammagamma/D");

  t->Branch("br_C_Wh",  &br_C_Wh,  "br_C_Wh/D");
  t->Branch("br_C_WH",  &br_C_WH,  "br_C_WH/D");
  t->Branch("br_C_tb",  &br_C_tb,  "br_C_tb/D");

  t->Branch("br_t_bC",  &br_t_bC,  "br_t_bC/D");
  t->Branch("br_C_taunu",  &br_C_taunu,  "br_C_taunu/D");

  t->Branch("oblique_S",  &oblique_S,  "oblique_S/D");
  t->Branch("oblique_T",  &oblique_T,  "oblique_T/D");
  t->Branch("oblique_U",  &oblique_U,  "oblique_U/D");
  t->Branch("oblique_V",  &oblique_V,  "oblique_V/D");
  t->Branch("oblique_W",  &oblique_W,  "oblique_W/D");
  t->Branch("oblique_X",  &oblique_X,  "oblique_X/D");

  t->Branch("type",  &type,  "type/I");
  t->Branch("status",  &status,  "status/I");


  // Begin by parsing the name to get the values of 
  // parameters used to generate the Sushi file
  
  TRegexp re_mh("mh=.*");
  TRegexp re_mH("mH=.*");
  TRegexp re_mA("mA=.*");
  TRegexp re_tb("tb=.*");
  TRegexp re_sba("sba=.*");
  TRegexp re_m122("m122=.*");
  TRegexp re_type("type=.*");
  TRegexp re_sqrts("sqrts=.*");
  TRegexp re_higgs("higgs=.*");

  mh = 0.0;
  mH = 0.0;
  mA = 0.0;
  mC = 0.0;
  tb = 0.0;
  sba = 0.0;
  type = 0;
  m12_2 = 0.0;


  for (int i = 0; i < sushifiles.size(); i++) {
    std::cout << "Processing SusHi file: " << sushifiles[i] << std::endl;
    TString tsushifile(sushifiles[i]);

    // Check for file existence before proceeding - skip files that
    // fail the check

    TString sushi_file_7TeV_0 = tsushifile;
    TString sushi_file_7TeV_1 = tsushifile.ReplaceAll("higgs=0","higgs=1");
    TString sushi_file_7TeV_2 = tsushifile.ReplaceAll("higgs=0","higgs=2");
    TString sushi_file_8TeV_0 = sushi_file_7TeV_0.ReplaceAll("7TeV","8TeV");
    TString sushi_file_8TeV_1 = sushi_file_7TeV_1.ReplaceAll("7TeV","8TeV");
    TString sushi_file_8TeV_2 = sushi_file_7TeV_2.ReplaceAll("7TeV","8TeV");
      
    if ( !(fileExists(sushi_file_7TeV_1.Data()) &&
	   fileExists(sushi_file_7TeV_2.Data()) &&
	   fileExists(sushi_file_8TeV_0.Data()) &&
	   fileExists(sushi_file_8TeV_1.Data()) &&
	   fileExists(sushi_file_8TeV_2.Data())) ) {
      std::cout << "WARNING: Found a file whose companions are missing: " << sushi_file_7TeV_0.Data() << std::endl;
      continue;
    }
	   
    
    mh = atof(MatchFromUntil(tsushifile(re_mh).Data(),"=","_").c_str());
    mA = atof(MatchFromUntil(tsushifile(re_mA).Data(),"=","_").c_str());
    mH = atof(MatchFromUntil(tsushifile(re_mH).Data(),"=","_").c_str());
    tb = atof(MatchFromUntil(tsushifile(re_tb).Data(),"=","_").c_str());
    sba = atof(MatchFromUntil(tsushifile(re_sba).Data(),"=","_").c_str());
    m12_2 = atof(MatchFromUntil(tsushifile(re_m122).Data(),"=","_").c_str());
    type = atof(MatchFromUntil(tsushifile(re_type).Data(),"=","_").c_str());
    
    mC = mH;
    // Take the stock parameters from the sushi file and fill the ROOT file
    get2HDMValues(sushifiles[i]);
    t->Fill();
    
/*
    if (m12_2 == 0.0) {
      for (mC = 300.0; mC <= 900.0; mC += 100.0) {
        if( mC == mH ) continue;
	get2HDMValues(sushifiles[i]);
	t->Fill();
      }
    } else {
      if (mH < 300.0) {
	mC = 300.0;
	get2HDMValues(sushifiles[i]);
	t->Fill();
      } else if (mH < 350.0) {
	mC = 350.0;
	get2HDMValues(sushifiles[i]);
	t->Fill();
      }
    } 
*/

    // compute the lepton-specific "type-III" values
    // simply by converting the type-I values 
    // with appropriate factors (e.g. http://arxiv.org/pdf/1106.0034v3.pdf,
    // page 12)
    
    if (type == 1) {
      type = 3;
    } else if (type == 2) {
      type = 4;
    }

    mC = mH;
    // Take the stock parameters from the sushi file and fill the ROOT file
    get2HDMValues(sushifiles[i]);
    t->Fill();
    
/*
    if (m12_2 == 0.0) {
      for (mC = 300.0; mC <= 900.0; mC += 100.0) {
        if( mC == mH ) continue;
	get2HDMValues(sushifiles[i]);
	t->Fill();
      }
    } else {
      if (mH < 300.0) {
	mC = 300.0;
	get2HDMValues(sushifiles[i]);
	t->Fill();
      } else if (mH < 350.0) {
	mC = 350.0;
	get2HDMValues(sushifiles[i]);
	t->Fill();
      }
    } 
*/
  }

  t->Write();
  f->Close();
}

int main(int argc, char* argv[]) {

  std::vector<string> sushifiles;

  // load 4FS functiona
  (*_4fs_xsec)["scalar_7TeV"] = static_cast<TF1*>((new TFile("4f_scalar_7.root"))->Get("xsec4f"));
  (*_4fs_xsec)["pseudoscalar_7TeV"] = static_cast<TF1*>((new TFile("4f_pseudoscalar_7.root"))->Get("xsec4f"));
  (*_4fs_xsec)["scalar_8TeV"] = static_cast<TF1*>((new TFile("4f_scalar_8.root"))->Get("xsec4f"));
  (*_4fs_xsec)["pseudoscalar_8TeV"] = static_cast<TF1*>((new TFile("4f_pseudoscalar_8.root"))->Get("xsec4f"));

  int opt;
  while ((opt = getopt(argc, argv, "s:h")) != -1) {
    switch (opt) {
    case 's':
      {
	sushifiles.push_back(optarg);
	break;
      }      

    default: /* '?' */
      fprintf(stderr, "Usage: %s [-p <sushi output file>] ... <output root file name>\n",
	      argv[0]);
      exit(EXIT_FAILURE);
    }
  }

  // Load the output ROOT file name
  if (optind >= argc) {
    fprintf(stderr, "You must pass a filename for the output ROOT file after options\n");
    exit(EXIT_FAILURE);
  }

  _outputfile = argv[optind];

  WriteRootFile(sushifiles);
  return 0;
}
